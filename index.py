from flask import  Flask, render_template, redirect
import pandas as pd
app = Flask(__name__)
@app.route('/')
@app.route('/index')

def index():
     return render_template('index.html')
   
    

@app.route('/table')
def tabela():
    
     df = pd.read_csv('noticia.csv')
     return df.to_html()

if __name__=="__main__":
    app.run(debug=True)