# web crawler

Web Crawler que busca notícias de crimes no estado do Pará no diario de notícias DOL.

Esse pequeno sistema e feita em Python, utilizando bibliotecas que facilitam chegar no objetivo sem muitos desvios. 

## Bibliotecas 
<p>Selenium : é um ótimo framework para realizar diversos tipos de tarefas com o browser.</p>
<p>Beautiful :  é uma biblioteca Python para extrair dados de arquivos HTML e XML.</p>
<p>Flask : é um micro framework para desenvolvimento web.</p>
<p>Pandas : é uma bibliote utilizado para manipulação e criação de dataframes.</>

## Sistema
<p> No momento o sistema por meio do web crawler  extrai os dados do arquivo HTML site dol, os dados são tratados e salvo em um csv.</p>
<p>O Sistema precisa de uma atualização para tratar os dados e salvar no banco de dados sqlite e assim mostrar o mesmo no pequeno aplicação web para visualizar a tabela do conteúdo extraido<p>
